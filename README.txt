whirld

a chat application written with ws and node

setup:
- clone repo
- make sure you have the latest versions of npm and node
- run npm install in the root directory
- run npm run build in the root directory
  this will start up a static http server running on port 80 as well as the websocket server hooked into it
- with an nginx configuration:
    server {
        listen 80;
        server_name 172.105.21.59;

        location / {
            proxy_pass http://127.0.0.1:8080;
            proxy_http_version 1.1;
            proxy_set_header Upgrade $http_upgrade;
            proxy_set_header Connection 'upgrade';
            proxy_set_header Host $host;
            proxy_cache_bypass $http_upgrade;
        }
    }
- enjoy!
