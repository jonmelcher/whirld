const webSocket = require('ws');
const connect = require('connect');
const http = require('http');
const serveStatic = require('serve-static');

/* create application and serve the public folder statically */
const app = connect();
app.use(serveStatic(__dirname + '/public'));

/* create the server over the app listening on port 80 to any IP */
const server = http.createServer(app).listen(8080, '127.0.0.1');

/* create the WebSocket server bound to the aforementioned server */
const wss = new webSocket.Server({
    server: server
});

/**
 * broadcast - broadcast to all listening clients of the WebSocket server
 *
 * @param {*} message
 */
const broadcast = (message) => {
    wss.clients.forEach((client) => {
        if (client.readyState === webSocket.OPEN) {
            client.send(message, (error) => {});
        }
    });
};

wss.on('connection', (ws) => {
    ws.on('message', broadcast);
});
