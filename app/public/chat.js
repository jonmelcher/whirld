/**
 * WhirldChat - main chatroom object
 *
 * @param {WebSocket} webSocket
 * @param {element} chatroom
 * @constructor
 */
function WhirldChat(webSocket, chatroom) {
    this.chatroom = chatroom;                       /* container element for chat message divs */
    this.users = {};                                /* id -> div map */
    this.client = new WhirldClient(webSocket);      /* wrapper for working with websocket */
    this.bindWebSocket(webSocket);                  /* instance-specific bindings for websocket */
    this.whirl = this.whirl.bind(this);
    this.whirlUser = this.whirlUser.bind(this);
    this.time = Date.now();
    this.dx = 1;
    requestAnimationFrame(this.whirl);              /* continuous whirling movement by chatroom */
}

/**
 * onKeyPress - keypress handler
 *
 * @param {Event} e
 */
WhirldChat.prototype.onKeyPress = function(e) {
    e.preventDefault();
    /**
     * if user has pressed enter, generate a new ID.
     * this will cause a new div to be created on next message
     * */
    if (e.which === 13) {
        this.client.generateId();
    }
    /* all other events get sent as messages to the websocket */
    else {
        this.client.send(String.fromCharCode(e.which));
    }
};

WhirldChat.prototype.onPaste = function(e) {
    e.preventDefault();
    var message = e.clipboardData.getData('text/plain');
    if (message.length > 0) {
        this.client.send(message);
    }
}

/**
 * onKeyDown - keydown handler
 *
 * @param {Event} e
 */
WhirldChat.prototype.onKeyDown = function(e) {
    /**
     * if user has pressed backspace, send a backspace token through the websocket
     * a different event than keypress is required since backspace is not recognized
     * by keypress, and keyup/keydown are case insensitive so are not useful otherwise
     */
    if (e.which === 8) {
        e.preventDefault();
        this.client.send({ backspace: true });
    }
}

/**
 * backspace - attempt to remove the last character from the identified user's div
 *
 * @param {string} id
 */
WhirldChat.prototype.backspace = function(id) {
    if (!this.users[id] || !this.users[id].div) {
        return;
    }
    var div = this.users[id].div;
    var text = div.innerText;
    div.innerText = text.substring(0, text.length - 1);
};

/**
 * bindWebSocket - bind instance specific event-handlers to the given WebSocket
 *
 * @param {WebSocket} webSocket
 */
WhirldChat.prototype.bindWebSocket = function(webSocket) {
    webSocket.onopen = function() {
        document.addEventListener('keypress', this.onKeyPress.bind(this));
        document.addEventListener('keydown', this.onKeyDown.bind(this));
        document.addEventListener('paste', this.onPaste.bind(this));
    }.bind(this);

    /* it might be worth creating some sort of Action object if we keep enlarging this if-else chain */
    webSocket.onmessage = function(message) {
        var parsed = JSON.parse(message.data);
        if (!parsed.message.backspace) {
            this.chat(parsed);
        }
        else {
            this.backspace(parsed.id);
        }
    }.bind(this);
};

/**
 * whirlUser - attempt to reposition a user's div, destroying it if it has reached the end of the chatroom container
 *
 * @param {object} user
 */
WhirldChat.prototype.whirlUser = function(user) {
    if (!user.div) {
        return;
    }
    var leftPosition = Number(user.div.style.left.replace('px', ''));
    user.div.style.left = (leftPosition - this.dx) + 'px';
    var rightPosition = user.div.getBoundingClientRect().right;
    var chatLeftPosition = this.chatroom.getBoundingClientRect().left;
    if (rightPosition <= chatLeftPosition) {
        this.chatroom.removeChild(user.div);
        delete user.div;
    }
};

/**
 * whirld - whirlUser each user in the chatroom
 */
WhirldChat.prototype.whirl = function() {
    var now = Date.now();
    this.dx = Math.floor((now - this.time) / 16) || 1;
    this.time = now;
    Object.values(this.users).forEach(this.whirlUser);
    requestAnimationFrame(this.whirl);
};

/**
 * chat - add or append to a user's div in the chatroom container the given message
 *
 * @param {string} message
 */
WhirldChat.prototype.chat = function(message) {
    if (!this.users[message.id]) {
        this.users[message.id] = {};
    }
    if (this.users[message.id].div) {
        this.users[message.id].div.innerHTML += message.message;
        return;
    }

    var newDiv = document.createElement('div');
    var brutalColour = generateBrutalColour();
    var complementaryColour = generateComplementaryColour(brutalColour);

    newDiv.style.background = brutalColour;
    newDiv.style.color = complementaryColour;
    newDiv.innerHTML = message.message;

    this.users[message.id].div = newDiv;
    this.chatroom.appendChild(newDiv);
};

/**
 * generateBrutalColour - randomly creates a hexstring in the brutalism palette
 *
 * @return {string}
 */
function generateBrutalColour() {
    var brutalChar = Math.floor(Math.random() * 16).toString(16);
    var colorArr = [ brutalChar + brutalChar, '00', 'ff' ].sort(function() {
        return Math.random() - 0.5;
    });
    return '#' + colorArr.join('');
};

function generateComplementaryColour(colour) {
    var parsed = parseInt(colour.substring(1), 16);
    var pad = '000000';
    var complementary = (0xffffff ^ parsed).toString(16);
    return '#' + pad.substring(0, pad.length - complementary.length) + complementary;
};
