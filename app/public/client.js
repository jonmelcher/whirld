/**
 * dec2hex - converts a decimal number to a hexadecimal string
 *
 * @param {*} dec
 * @return {string}
 */
function dec2hex(dec) {
    return ('0' + dec.toString(16)).substr(-2);
}

/**
 * WhirldClient - thin wrapper over a WebSocket
 *
 * @param {WebSocket} webSocket
 * @constructor
 */
function WhirldClient(webSocket) {
    this.socket = webSocket;    /* passed in initialized WebSocket */
    this.generateId();          /* creates a new random id for identifying the client */
}

/**
 * send - sends a JSON-encoded message to the WebSocket including the id of the client
 *
 * @param {*} message
 */
WhirldClient.prototype.send = function(message) {
    this.socket.send(JSON.stringify({
        id: this.id,
        message: message
    }));
};

/**
 * generateId - sets a random hexadecimal id on the instance to identify itself to the WebSocket
 */
WhirldClient.prototype.generateId = function() {
    var arr = new Uint8Array(5);
    window.crypto.getRandomValues(arr);
    this.id = Array.from(arr).map(dec2hex).join('');
};
